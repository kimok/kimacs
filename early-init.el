(defun init-path (path) (concat user-emacs-directory path))

(setq kimacs-config-file (init-path "init.org")
      kimacs-init-file (init-path "init.el"))

(setq custom-file (init-path "custom.el"))
(when (not (file-exists-p custom-file))
  (shell-command (concat "touch " custom-file))
  (load custom-file))

(defcustom kimacs-branch "master" "TODO: document kimacs branch")

(defcustom kimacs-author-mode nil "Kimacs for authors. Designed around org-mode.")
(defcustom kimacs-programmer-mode nil "Kimacs for programmers.")
(defcustom kimacs-beginner-mode nil "Kimacs for beginner programmers.")

(defun load-init (path)
  (when (file-exists-p (init-path path))
    (load-file (init-path path))))

(load-init "init-straight.el")
(straight-use-package 'org)

(require 'org)
(org-babel-tangle-file (init-path "init.org"))
