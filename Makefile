KIMACS = $(HOME)/.config/kimacs

install:
	mkdir -p $(KIMACS)
	$(MAKE) install-straight
	$(MAKE) install-literate-config

install-literate-config:
	cp init.org $(KIMACS)/init.org.tmp
	rm -f $(KIMACS)/init.org $(KIMACS)/init.el
	mv $(KIMACS)/init.org.tmp $(KIMACS)/init.org
	cd $(KIMACS) &&  emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "init.org")'

install-straight:
	cp init-straight.el $(KIMACS)/init-straight.el.tmp
	rm -f $(KIMACS)/init-straight.el
	mv $(KIMACS)/init-straight.el.tmp $(KIMACS)/init-straight.el
