(("autothemer" . "1dbc06ad430c51b5ec1a602a808ee46b9bd4bafa")
 ("boxy" . "777e681ba8e5133967fb2d03862983280882e468")
 ("boxy-headings" . "1ae1fe168f4cc002d8e21d24b6aed6010d6380fb")
 ("compat" . "884d47ef896cff2fa6910c359f1aa0e0ff8c7acd")
 ("dash.el" . "0ac1ecf6b56eb67bb81a3cf70f8d4354b5782341")
 ("el-get" . "bf3dba444dcd240b8cb358a0850c8c5a92606134")
 ("emacs-theme-gruvbox" . "921bfd7a2f5174b68682b04e6010b156bbfe6c70")
 ("emacs-which-key" . "1ab1d0cc88843c9a614ed3226c5a1070e32e4823")
 ("emacsmirror-mirror" . "634e620eaa04afb10ea292bd039137667fe546d4")
 ("general.el" . "9651024e7f40a8ac5c3f31f8675d3ebe2b667344")
 ("gnu-elpa-mirror" . "808923d95777d378ca340b8020dd571e6a62460a")
 ("leaf-keywords.el" . "849b579f87c263e2f1d7fb7eda93b6ce441f217e")
 ("leaf.el" . "9eb18e8c9c375aa0158fbd06ea906bfbf54408fe")
 ("magit" . "2dfeaa6839c643a54d96c9f855bae11d5cba2453")
 ("melpa" . "5be08102d4de84c35b18cd1e3321fe8b6836ff56")
 ("orderless" . "8b9af2796fa0eb87eea4140bc08d16880a493803")
 ("org" . "ae168d5c8c26023643af97ea25f9947c85324da1")
 ("rainbow-delimiters" . "a32b39bdfe6c61c322c37226d66e1b6d4f107ed0")
 ("rainbow-identifiers" . "19fbfded1baa98d12335f26f6d7b20e5ae44ce2e")
 ("restart-emacs" . "1607da2bc657fe05ae01f7fdf26f716eafead02c")
 ("straight.el" . "4517e118ee43f849f708025dbb2cf4f281793121")
 ("transient" . "2e4426fe8161893382f09b3f4635e152ee02488e")
 ("vertico" . "755a56063ed1626b9a89c2a20d69a30fda9e7663")
 ("visual-fill-column" . "453d698d7fc243a547665f8ba43c55eee574e0db")
 ("with-editor" . "cfcbc2731e402b9169c0dc03e89b5b57aa988502"))
:beta
