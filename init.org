* Init
When emacs starts, it evaluates everything in init.el.
#+begin_src emacs-lisp :tangle init.el
(custom-set-variables '(native-comp-async-report-warnings-errors nil))
#+end_src

** Install leaf
Leaf is a declarative package configurator.
#+begin_src emacs-lisp :tangle init.el
(eval-and-compile
  (straight-use-package 'leaf)
  (straight-use-package 'leaf-keywords)
  (leaf-keywords-init))
#+end_src
#+begin_src emacs-lisp :tangle init.el
(custom-set-variables '(leaf-defaults '(:straight t)))

#+end_src
** Determine kimacs' mode
Author & programmer modes are persistent.
Run the ~kimacs --author~ or ~kimacs --programmer~ to change the mode.
By default, kimacs starts in programmer mode.
#+begin_src emacs-lisp :tangle init.el
(when (member "--author" command-line-args)
  (custom-set-variables '(kimacs-author-mode nil t)
			'(kimacs-programmer-mode t t)))

(add-to-list
 'command-switch-alist '("--author" .
			 (lambda (switch)
			   (customize-save-variable 'kimacs-author-mode t)
			   (customize-save-variable 'kimacs-programmer-mode nil))))

(when (member "--programmer" command-line-args)
  (custom-set-variables '(kimacs-author-mode nil t)
			'(kimacs-programmer-mode t t)))

(add-to-list
 'command-switch-alist '("--programmer" .
			 (lambda (switch)
			   (customize-save-variable 'kimacs-author-mode nil)
			   (customize-save-variable 'kimacs-programmer-mode t))))

;; (defun kimacs-first-time-author-mode ()
;;   (when (and
;;	 (not (boundp 'kimacs-first-time))
;;	 (not kimacs-programmer-mode))
;;     (customize-save-variable 'kimacs-author-mode nil)
;;     (customize-save-variable 'kimacs-first-time nil)))

;; (add-hook 'kimacs-after-custom-load-hook #'kimacs-first-time-author-mode)
#+end_src
** Set up environment
#+begin_src emacs-lisp :tangle init.el
(leaf exec-path-from-shell
  :config
  (exec-path-from-shell-initialize))
#+end_src
* Base configuration
Code that is always run
** Self-documenting keybindings
- which-key :: shows a menu of the next possible key
#+begin_src emacs-lisp :tangle init-base.el
(leaf which-key
  :custom (which-key-mode . t))
#+end_src
- general :: binds keys
#+begin_src emacs-lisp :tangle init-base.el
(leaf general
  :config
  (require 'general)
  (general-create-definer leader-key
    :prefix "<escape>"))
#+end_src
- marginalia :: explains functions & their keybindings
#+begin_src emacs-lisp :tangle init-base.el
(leaf marginalia
  :config
  (marginalia-mode t))
#+end_src
** macOS improvements
#+begin_src emacs-lisp :tangle init-base.el
(when (eq system-type 'darwin)
  (setq mac-command-key-is-meta nil
	mac-option-key-is-meta t ;; is-ctrl
	mac-command-modifier 'super
	mac-option-modifier 'meta))
#+end_src

** Navigate windows & buffers
#+begin_src emacs-lisp :tangle init-base.el
(winner-mode)
(general-define-key
  "M-u" 'winner-undo
  "C-M-u" 'winner-redo
  "M-i" 'windmove-up
  "M-j" 'windmove-left
  "M-l" 'windmove-right
  "M-k" 'windmove-down
  "M-o" 'other-window)
(leader-key
  "b" 'switch-to-buffer)
#+end_src
*** Resize the UI
#+begin_src emacs-lisp :tangle init-base.el
(general-define-key
 "C-+" 'global-text-scale-adjust)
#+end_src
** Visual style
*** Rainbows
#+begin_src emacs-lisp :tangle init-base.el
(leaf rainbow-delimiters
  :hook
  (prog-mode-hook . rainbow-delimiters-mode))
#+end_src
#+begin_src emacs-lisp :tangle init-base.el
(leaf rainbow-identifiers
  :hook
  (prog-mode-hook . rainbow-identifiers-mode))
#+end_src
#+begin_src emacs-lisp :tangle init-base.el
(leaf rainbow-mode
  :hook
  (org-mode-hook . rainbow-mode))
#+end_src


*** Emacs theme
#+begin_src emacs-lisp :tangle init-base.el
(leaf gruvbox-theme
  :config
  (load-theme 'gruvbox-dark-soft t)
  (push '("interactive" . ?⌨) lisp-prettify-symbols-alist)
  (global-prettify-symbols-mode 1)
  :custom
  (menu-bar-mode . nil)
  (tool-bar-mode . nil)
  (tool-tip-mode . nil)
  (global-hl-line-mode . t)
  (org-fontify-quote-and-verse-blocks . t)
  (blink-cursor-mode . nil)
  :custom-face
  ((org-block-begin-line org-block-end-line) .
   '((t (:extend t :background "#222222"
		 :foreground "#555555" :height 0.5))))
  ((org-block org-quote) .
   '((t (:extend t :background "#222222" :height 0.9))))
  (hl-line . '((t (:extend t :background "#161616"))))
  (cursor . '((t (:background "MediumOrchid1" :inverse-video t)))))
#+end_src

*** Misc
#+begin_src emacs-lisp :tangle init-base.el
(custom-set-variables '(display-buffer-base-action '(nil)))
#+end_src
** Org-mode tweaks
- Align source blocks without indentation
#+begin_src emacs-lisp :tangle init-base.el
(customize-set-variable 'org-src-preserve-indentation t)
#+end_src

- Open links with a mouse click
#+begin_src emacs-lisp :tangle init-base.el
(customize-set-variable 'org-mouse-1-follows-link t)
#+end_src

- Show org documents in a centered column
#+begin_src emacs-lisp :tangle init-base.el
(leaf visual-fill-column
  :hook
  (org-mode-hook . visual-fill-column-mode)
  (org-mode-hook . toggle-truncate-lines)
  (org-mode-hook . visual-line-mode)
  :custom
  (visual-fill-column-center-text . t)
  (visual-fill-column-width . 100)
  (org-auto-align-tags . nil)
  (org-tags-column . 0)
  :config
  (leader-key "<tab>" 'visual-fill-column-mode))

(leader-key "o" '(:ignore t :which-key "Org-mode")
  "ol" '(org-insert-link :which-key "Insert link")
  "oL" '(org-store-link :which-key "Store link")
  "oe" '(org-toggle-link-display :which-key "Expand links"))
#+end_src
- Don't linkify tags
  this confuses our users who just want to edit tag text.
#+begin_src emacs-lisp :tangle init-base.el
(customize-set-variable
 'org-highlight-links '(bracket angle plain radio date footnote))
#+end_src

- Hide emphasis
  Hide delimiters for =*bold*, +strikethrough+= etc.
#+begin_src emacs-lisp :tangle init-base.el
(customize-set-variable
 'org-hide-emphasis-markers t)
#+end_src

- Drag and drop files
  For projects like [[kimok/interactive-cv]], dragging a file into an org buffer will copy it into a nearby directory and insert a link.
#+begin_src emacs-lisp :tangle init-base.el
(leaf org-download
  :config
  (require 'org-download)
  :custom
  (org-download-display-inline-images . nil))
#+end_src

- Fancy lists
#+begin_src emacs-lisp :tangle init-base.el
'(org-cycle-include-plain-lists 'integrate)
'(org-ellipsis " ⚟")
#+end_src

** Customize kimacs
#+begin_src emacs-lisp :tangle init-base.el
(defun kimacs-open-user-init () (interactive)
    (find-file (init-path "init-user.el")))
(leader-key
  "ku" 'kimacs-open-user-init)
#+end_src
** Take notes
*** Annotate with org-remark
#+begin_src emacs-lisp :tangle init-base.el
(if (>= emacs-major-version 29)
    (leaf org-remark
      :config
      (leader-key
	"n"  '(:ignore t  :which-key "Notes & Narrowing")
	"nN" '(org-remark-mark-blue :which-key "Add a Note")
	"nM" '(org-remark-mark-green :which-key "Add a Note (green)")
	"n<" '(org-remark-mark-red :which-key "Add a Note (red)")
	"nn" '(org-remark-open :which-key "Open note")
	"nK" '(org-remark-delete :which-key "Delete note")
	"nc" '(org-remark-change :which-key "Change note style")
	"ni" '(org-remark-view-prev :which-key "Open previous note")
	"nk" '(org-remark-view-next :which-key "Open next note"))
      (org-remark-global-tracking-mode +1)
      (require 'org-remark)
      (org-remark-create "red"
			 '(:background "#443333")
			 '(highlight "red"))
      (org-remark-create "green"
			 '(:background "#334444")
			 '(highlight "green"))
      (org-remark-create "blue"
			 '(:background "#333344")
			 '(highlight "blue"))
      :custom
      (org-remark-notes-file-name . 'org-remark-notes-file-name-function)
      (org-remark-source-file-name . 'abbreviate-file-name)))
#+end_src

*** Jump to note files
#+begin_src emacs-lisp :tangle init-base.el
(defun kimacs-open-user-doc (filename)
       (find-file (concat "~/org/" filename)))
(leader-key
  "fes" #'(lambda () (interactive)
	    (kimacs-open-user-doc "pers.org"))
  "fet" #'(lambda () (interactive)
	    (kimacs-open-user-doc "todo.org")))
#+end_src
** Completion
#+begin_src emacs-lisp :tangle init-base.el
(leaf vertico
  :config
  (leaf orderless
    :custom (completion-styles . '(orderless)))
  :custom
  (vertico-mode . t)
  (completion-category-defaults . nil)
  (completion-category-overrides . '((file (styles partial-completion)))))
#+end_src
** Projects
*** Develop with git
#+begin_src emacs-lisp :tangle init-base.el
(leaf magit
  :config (leader-key "g"  '(:ignore t :which-key "git")
	    "gs" 'magit-status
	    "gb" 'magit-blame
	    "gn" 'magit-blob-visit-file))
#+end_src
*** Collaborate
#+begin_src emacs-lisp :tangle init-base.el
(leader-key "ln" '(global-linum-mode :which-key "Line numbers"))

(leaf git-link
  :config
  (leaf simpleclip)
  (leader-key "gl" 'git-link)
  (defun git-link-to-clipboard (f &rest args)
    (simpleclip-set-contents (car args))
    (apply f args))
  (advice-add 'git-link--new :around #'git-link-to-clipboard))
#+end_src

*** Update projects
#+begin_src emacs-lisp :tangle init-base.el
(leaf restart-emacs)
(defun kimacs-project-update (directory branch &optional restart) (interactive)
       (cd directory)
       (unwind-protect
	   (magit-stash-worktree "kimacs-project autostash")
	 (call-process "git" nil nil nil "fetch")
	 (call-process "git" nil nil nil "checkout" branch)
	 (call-process "git" nil nil nil "rebase" "--autostash" "origin/master")
	 (when restart (kimacs-reload) (restart-emacs))))
#+end_src

*** kimok/interactive-cv
#+begin_src emacs-lisp :tangle init-base.el
(defcustom kimacs-project-cv-path (expand-file-name "~/interactive-cv") "The root directory of the interactive-cv project")
(defcustom kimacs-project-cv-content-branch "ashaw" "The branch to which the user should add  content. The function `kimacs-project-cv-publish` will publish anything in the corresponding content directory to this branch. For instance, changes `src/content/kimok/content.org` will be added to the `kimok` branch.")
(defcustom kimacs-project-cv-build "ashaw" "The build key for shadow-cljs. Should usually match the value of `kimacs-project-cv-content-branch`.")
(add-to-list 'dir-locals-directory-cache
	     `(,kimacs-project-cv-path /home/kk/proj/interactive-cv nil))
#+end_src

#+begin_src emacs-lisp :tangle init-base.el
(defun kimacs-project-cv-update () (interactive)
       (kimacs-project-update kimacs-project-cv-path kimacs-project-cv-content-branch))
(leader-key "pU" '(kimacs-project-cv-update :which-key "Update"))
#+end_src

#+begin_src emacs-lisp :tangle init-base.el
(defun kimacs-project-cv-start () (interactive)
       (let ((kill-buffer-query-functions nil))
	 (kill-matching-buffers "*cider" nil t))
       (cider-jack-in-cljs (list :project-dir kimacs-project-cv-path)))

(defun kimacs-project-cv-install () (interactive)
       (cd (expand-file-name "~"))
       (call-process "git" nil nil nil "clone"
		     "https://gitlab.com/kimok/interactive-cv.git"
		     kimacs-project-cv-path)
       (cd kimacs-project-cv-path)
       (call-process "npm" nil nil nil "install"))
(leader-key
  "pI" '(kimacs-project-cv-install :which-key "Install"))

(defun kimacs-project-cv-open-in-browser () (interactive)
       (browse-url "http://localhost:8081"))

(defun kimacs-project-cv-open-content-file () (interactive)
  (find-file (concat kimacs-project-cv-path "/src/content/"
		     kimacs-project-cv-build "/content.org")))

(leader-key
  "p" '(:ignore t :which-key "Project")
  "pb" '(kimacs-project-cv-open-in-browser
	 :which-key "Open in browser")
  "ps" '(kimacs-project-cv-start
	 :which-key "Start server")
  "pf" '((lambda () (interactive)
	   (cd kimacs-project-cv-path)
	   (project-find-file))
	 :which-key "Find file")
  "pg" '(project-find-regexp
         :which-key "Find regex")
  "pc" '(kimacs-project-cv-open-content-file
	 :which-key "Open content file"))
#+end_src

#+begin_src emacs-lisp :tangle init-base.el
(defun kimacs-project-cv-publish () (interactive)
       (cd kimacs-project-cv-path)
       (call-process "git" nil nil nil
		     "add" (concat "src/content/"
				   kimacs-project-cv-build
				   "/*"))
       (call-process "git" nil nil nil "commit" "-m" "content update")
       (magit-git-push kimacs-project-cv-content-branch
		       (concat "origin/" kimacs-project-cv-content-branch)
		       nil))

(leader-key "pP" '(kimacs-project-cv-publish
		   :which-key "Publish"))
#+end_src

*** kimok/kimacs
**** Update kimacs
#+begin_src emacs-lisp :tangle init-base.el
(defun kimacs-update () (interactive)
       (kimacs-project-update user-emacs-directory "origin/master" 'restart))

(leader-key "k" '(:ignore t :which-key "Kimacs")
  "kr" 'kimacs-reload
  "kU" 'kimacs-update)
#+end_src
**** Edit kimacs
#+begin_src emacs-lisp :tangle init-base.el
(leader-key
  "k"  '(:ignore t :which-key "kimacs")
  "kk" 'kimacs-config
  "kR" 'kimacs-reset)
#+end_src
**** Open
#+begin_src emacs-lisp :tangle init-base.el
(defun kimacs-config () (interactive) (find-file kimacs-config-file))
#+end_src
**** Reload kimacs
#+begin_src  emacs-lisp :tangle init-base.el
(defun kimacs-reload () (interactive)
       (require 'org)
       (org-babel-tangle-file kimacs-config-file)
       (load-file kimacs-init-file))
#+end_src
**** Reset kimacs state
#+begin_src  emacs-lisp :tangle init-base.el
(defun kimacs-reset () (interactive)
(delete-file (concat user-emacs-directory "custom.el"))
(restart-emacs))
#+end_src
** Easily develop clojure projects
#+begin_src emacs-lisp :tangle init-base.el
(leader-key
  "c" '(:ignore t :which-key "clojure")
  "cr" 'raise-sexp
  "r"  'raise-sexp
  "ct" 'transpose-sexps
  "clb" 'cider-load-buffer
  "u" 'clojure-unwind)
#+end_src

- use cider
#+begin_src emacs-lisp :tangle init-base.el
(leaf cider
  :config
  (leaf cider-eval-sexp-fu
    :config (require 'cider-eval-sexp-fu))
  (leader-key "cj" 'cider-jack-in-cljs)
  (leader-key "cJ" 'cider-jack-in))
#+end_src
- simplify cider's UX
#+begin_src emacs-lisp :tangle init-base.el
(defun kimacs-cider-restart () (interactive)
       (call-interactively
	(kill-matching-buffers (rx "*cider-repl") nil 'no-ask)
	t (vector "yes")))

(custom-set-variables
 '(cider-offer-to-open-cljs-app-in-browser nil)
 '(cider-repl-pop-to-buffer-on-connect nil)
 '(safe-local-variable-values
   '((cider-repl-pop-to-buffer-on-connect . nil)
     (cider-offer-to-open-cljs-app-in-browser . nil)
     (cider-shadow-default-options . (concat ":" kimacs-interactive-cv-build))
     (cider-default-cljs-repl . shadow)
     (cider-preferred-build-tool . shadow-cljs))))

(leader-key "cr" '(kimacs-cider-restart :which-key "restart clojurescript"))
#+end_src
** Edit code intelligently
#+begin_src emacs-lisp :tangle init-base.el
(general-define-key
 "C-M-t" 'transpose-sexps)
(leader-key
 "j" #'(lambda () (interactive) (progn (next-line) (join-line))))
#+end_src
*** Fold code
#+begin_src emacs-lisp :tangle init-base.el
(add-hook 'prog-mode-hook #'hs-minor-mode)
(leader-key
  "nf" '(hs-hide-all :which-key "Fold all")
  "nl" '(hs-hide-level :which-key "Fold level")
  "nb" '(hs-hide-block :which-key "Fold block")
  "no" '(hs-show-block :which-key "Unfold block")
  "nd" '(narrow-to-defun :which-key "Narrow to definition"))
#+end_src
*** Align & Format
#+begin_src emacs-lisp :tangle init-base.el
(add-hook 'prog-mode-hook
	  (lambda ()
	    (add-hook 'before-save-hook
		      'eglot-format-buffer
		      0 'local)))
#+end_src
*** Handle whitespace
#+begin_src emacs-lisp :tangle init-base.el
(customize-set-variable 'indent-tabs-mode nil)
(add-hook 'prog-mode-hook
	  (lambda () (add-hook
		      'before-save-hook
		      'whitespace-cleanup nil t)))
#+end_src
*** Investigate buffer history
#+begin_src emacs-lisp :tangle init-base.el
(leaf vundo)
#+end_src
*** Explore projects
#+begin_src emacs-lisp :tangle init-base.el
(leader-key
  "pf" 'project-find-file
  "pg" 'project-find-regexp
  "sup" 'straight-use-package)
#+end_src
** Store backup files
#+begin_src emacs-lisp :tangle init-base.el
(custom-set-variables '(backup-directory-alist '((".*" . "/tmp"))))
#+end_src
* For Authors
A text editor just for writing content. Not for learning emacs.
** Define author mode
#+begin_src emacs-lisp :tangle init.el
;;;###autoload
(define-minor-mode kimacs-author-mode
  "kimacs author mode"
  :lighter " kk/B"
  :global t
  :set (lambda (n v)
	 (set-default n v)
	 (when v (load-init "init-author.el"))))
#+end_src
** Interaction
#+begin_src emacs-lisp :tangle init-author.el
(setq inhibit-startup-screen t)
(setq inhibit-startup-echo-area-message t)
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
(setq frame-resize-pixelwise t)
(global-auto-revert-mode)
(tool-bar-mode 0)
(tooltip-mode)
(show-paren-mode nil)
(scroll-bar-mode 0)
(save-place-mode 1)
(setq auto-save-default nil)
(auto-save-visited-mode +1)
(straight-use-package 'adaptive-wrap)
(straight-use-package 'define-word)
#+end_src
** Keybindings
#+begin_src emacs-lisp :tangle init-author.el
(straight-use-package 'wakib-keys)
(wakib-keys 1)
(add-hook 'after-change-major-mode-hook 'wakib-update-major-mode-map)
(add-hook 'menu-bar-update-hook 'wakib-update-minor-mode-maps)
(define-key isearch-mode-map (kbd "C-f") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "C-S-f") 'isearch-repeat-backward)
(define-key isearch-mode-map (kbd "M-;") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "M-:") 'isearch-repeat-backward)
(define-key isearch-mode-map (kbd "C-v") 'isearch-yank-kill)
(define-key isearch-mode-map (kbd "M-d") 'isearch-delete-char)
(define-key text-mode-map (kbd "M-#") 'define-word)
(general-unbind 'wakib-keys-map "<escape>")
(general-unbind 'wakib-keys-overriding-map "<escape>")
#+end_src
** Aesthetics
#+begin_src emacs-lisp :tangle init-author.el

(setq-default line-spacing 2)
(setq x-underline-at-descent-line t)
(set-default 'cursor-type  '(bar . 5))
(setq visible-bell t)
(setq ring-bell-function 'ignore)

(fringe-mode '(0 . 0))


(defface fallback '((t :family (if (x-list-fonts "Fira Code Light") "Fira Code Light" nil)
		       :inherit 'face-faded)) "Fallback")
(set-display-table-slot standard-display-table 'truncation
			(make-glyph-code ?… 'fallback))
(set-display-table-slot standard-display-table 'wrap
			(make-glyph-code ?↩ 'fallback))
(set-display-table-slot standard-display-table 'selective-display
			(string-to-vector " …"))

(define-key mode-line-major-mode-keymap [header-line]
  (lookup-key mode-line-major-mode-keymap [mode-line]))

(defun mode-line-render (left right)
  (let* ((available-width (- (window-width) (length left) )))
    (format (format "%%s %%%ds" available-width) left right)))
(setq-default mode-line-format
	      '((:eval
		 (mode-line-render
		  (format-mode-line (list
				     (propertize "☰" 'face `(:inherit mode-line-buffer-id)
						 'help-echo "Mode(s) menu"
						 'mouse-face 'mode-line-highlight
						 'local-map   mode-line-major-mode-keymap)
				     " %b "
				     (if (and buffer-file-name (buffer-modified-p))
					 (propertize "(modified)" 'face `(:inherit face-faded)))))

		  (format-mode-line
		   (list
		    (propertize "[ L %l : C %c ]  " 'face `(:inherit face-faded))
		    (propertize
		     (format "%d Words  " (count-words (point-min) (point-max)))
		     'face `(:inherit face-faded))))))))

;; Comment if you want to keep the modeline at the bottom
(setq-default header-line-format mode-line-format)
(setq-default mode-line-format'(""))

(setq window-divider-default-right-width 3)
(setq window-divider-default-places 'right-only)
(window-divider-mode)

(defun set-face (face style)
  "Reset a face and make it inherit style."
  (set-face-attribute face nil
   :foreground 'unspecified :background 'unspecified
   :family     'unspecified :slant      'unspecified
   :weight     'unspecified :height     'unspecified
   :underline  'unspecified :overline   'unspecified
   :box        'unspecified :inherit    style))

(defgroup writer nil
  "Faces for the writer theme"
  :prefix "writer-face-"
  :group 'faces)
#+end_src
** Custom faces
#+begin_src emacs-lisp :tangle init-author.el
(defface face-faded nil
  "Used for menubars"
  :group 'writer)

(defun set-modeline-faces ()
  (set-face 'header-line                                 'face-strong)
  (set-face-attribute 'header-line nil
				:underline (face-foreground 'default))
  (set-face-attribute 'mode-line nil
		      :height 10
		      :underline (face-foreground 'face-faded)
		      :overline nil
		      :box nil
		      :foreground (face-background 'default)
		      :background (face-background 'default))
  (set-face 'mode-line-inactive                            'mode-line)
  (set-face-attribute 'cursor nil
		      :background (face-foreground 'default))
  (set-face-attribute 'window-divider nil
		      :foreground (face-background 'mode-line))
  (set-face-attribute 'window-divider-first-pixel nil
		      :foreground (face-background 'default))
  (set-face-attribute 'window-divider-last-pixel nil
		      :foreground (face-background 'default))
  )

(defun set-button-faces ()
  (set-face-attribute 'custom-button nil
		      :foreground (face-foreground 'face-faded)
		      :background (face-background 'face-subtle)
		      :box `(:line-width 1
			     :color ,(face-foreground 'face-faded)
			     :style nil))
  (set-face-attribute 'custom-button-mouse nil
		      :foreground (face-foreground 'default)
		      ;; :background (face-foreground 'face-faded)
		      :inherit 'custom-button
		      :box `(:line-width 1
			     :color ,(face-foreground 'face-subtle)
			     :style nil))
  (set-face-attribute 'custom-button-pressed nil
		      :foreground (face-background 'default)
		      :background (face-foreground 'face-salient)
		      :inherit 'face-salient
		      :box `(:line-width 1
			     :color ,(face-foreground 'face-salient)
			     :style nil)
		      :inverse-video nil))
#+end_src
* For Beginners
Presets for beginners learning emacs.
Beginners don't edit init files.

** Welcome buffer
- explain the leader menu
- link to [[https://orgmode.org/worg/org-tutorials/org4beginners.html#orgb94f796][Org mode beginning at the basics]]
- tutorials
- button :: how to set up init-programmer

** Keybindings
Emacs' builtin keymaps are disabled.

* Main entrypoints
#+begin_src emacs-lisp :tangle init.el
(load-init "init-base.el")
(load-file custom-file)
(run-hooks 'kimacs-after-custom-load-hook)
(when kimacs-programmer-mode
  (load-init "init-programmer.el")
  (when kimacs-beginner-mode (load-init "init-beginner.el")))
(load-init "init-user.el")
#+end_src
