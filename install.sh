config=$(dirname -- ${BASH_SOURCE[0]})
emacs -batch --exec "(progn (load-file \"${config}/early-init.el\") (load-file \"${config}/init.el\"))"
